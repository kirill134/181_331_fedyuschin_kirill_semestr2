/********************************************************************************
** Form generated from reading UI file 'adminq.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINQ_H
#define UI_ADMINQ_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_adminq
{
public:
    QLabel *label;

    void setupUi(QDialog *adminq)
    {
        if (adminq->objectName().isEmpty())
            adminq->setObjectName(QString::fromUtf8("adminq"));
        adminq->resize(400, 300);
        label = new QLabel(adminq);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(150, 120, 47, 13));

        retranslateUi(adminq);

        QMetaObject::connectSlotsByName(adminq);
    } // setupUi

    void retranslateUi(QDialog *adminq)
    {
        adminq->setWindowTitle(QApplication::translate("adminq", "Dialog", nullptr));
        label->setText(QApplication::translate("adminq", "1111111", nullptr));
    } // retranslateUi

};

namespace Ui {
    class adminq: public Ui_adminq {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINQ_H

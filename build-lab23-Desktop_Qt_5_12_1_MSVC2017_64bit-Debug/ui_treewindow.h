/********************************************************************************
** Form generated from reading UI file 'treewindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TREEWINDOW_H
#define UI_TREEWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_treewindow
{
public:
    QLabel *label_2;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_3;
    QListWidget *listWidget_3;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pushButton_2;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_4;
    QVBoxLayout *verticalLayout_4;
    QListWidget *listWidget_4;
    QPushButton *pushButton_4;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QTextBrowser *textBrowser;
    QWidget *tab_2;
    QTextBrowser *textBrowser_2;
    QWidget *tab_3;
    QTextBrowser *textBrowser_3;
    QWidget *tab_4;
    QTextBrowser *textBrowser_4;
    QPushButton *pushButton_5;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayout_7;
    QLabel *label;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *lineEdit;
    QTextBrowser *textBrowser_5;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QPushButton *pushButton;

    void setupUi(QDialog *treewindow)
    {
        if (treewindow->objectName().isEmpty())
            treewindow->setObjectName(QString::fromUtf8("treewindow"));
        treewindow->resize(686, 499);
        treewindow->setStyleSheet(QString::fromUtf8("background-color: rgb(215, 217, 255);"));
        label_2 = new QLabel(treewindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(30, 20, 251, 20));
        layoutWidget = new QWidget(treewindow);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(310, 40, 161, 161));
        verticalLayout_5 = new QVBoxLayout(layoutWidget);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("font: 75 9pt \"Source Code Pro\";"));

        verticalLayout_5->addWidget(label_3);

        listWidget_3 = new QListWidget(layoutWidget);
        listWidget_3->setObjectName(QString::fromUtf8("listWidget_3"));
        listWidget_3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout_5->addWidget(listWidget_3);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_2->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"  \n"
"background-color: rgb(0, 85, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(1,40,1,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        verticalLayout_3->addWidget(pushButton_2);


        verticalLayout_5->addLayout(verticalLayout_3);

        layoutWidget1 = new QWidget(treewindow);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(500, 40, 161, 161));
        verticalLayout_6 = new QVBoxLayout(layoutWidget1);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setStyleSheet(QString::fromUtf8("font: 75 9pt \"Source Code Pro\";"));

        verticalLayout_6->addWidget(label_4);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        listWidget_4 = new QListWidget(layoutWidget1);
        listWidget_4->setObjectName(QString::fromUtf8("listWidget_4"));
        listWidget_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout_4->addWidget(listWidget_4);

        pushButton_4 = new QPushButton(layoutWidget1);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_4->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"   \n"
"  \n"
"background-color: rgb(0, 85, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(1,40,1,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        verticalLayout_4->addWidget(pushButton_4);


        verticalLayout_6->addLayout(verticalLayout_4);

        layoutWidget2 = new QWidget(treewindow);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(30, 50, 251, 151));
        verticalLayout = new QVBoxLayout(layoutWidget2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(layoutWidget2);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setStyleSheet(QString::fromUtf8(" border-radius: 5px;"));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Triangular);
        tabWidget->setIconSize(QSize(20, 20));
        tabWidget->setElideMode(Qt::ElideNone);
        tabWidget->setTabBarAutoHide(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        textBrowser = new QTextBrowser(tab);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 251, 111));
        textBrowser->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        textBrowser_2 = new QTextBrowser(tab_2);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(-1, -1, 251, 111));
        textBrowser_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        textBrowser_3 = new QTextBrowser(tab_3);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));
        textBrowser_3->setGeometry(QRect(0, -1, 251, 111));
        textBrowser_3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));
        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        textBrowser_4 = new QTextBrowser(tab_4);
        textBrowser_4->setObjectName(QString::fromUtf8("textBrowser_4"));
        textBrowser_4->setGeometry(QRect(0, 0, 251, 111));
        textBrowser_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));
        tabWidget->addTab(tab_4, QString());

        verticalLayout->addWidget(tabWidget);

        pushButton_5 = new QPushButton(layoutWidget2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_5->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"  \n"
"background-color: rgb(0, 85, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(1,40,1,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        verticalLayout->addWidget(pushButton_5);

        layoutWidget3 = new QWidget(treewindow);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(250, 220, 251, 221));
        verticalLayout_7 = new QVBoxLayout(layoutWidget3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget3);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("font: 8pt \"Impact\";"));

        verticalLayout_7->addWidget(label);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lineEdit = new QLineEdit(layoutWidget3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setTabletTracking(false);
        lineEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout_2->addWidget(lineEdit);

        textBrowser_5 = new QTextBrowser(layoutWidget3);
        textBrowser_5->setObjectName(QString::fromUtf8("textBrowser_5"));
        textBrowser_5->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout_2->addWidget(textBrowser_5);

        checkBox = new QCheckBox(layoutWidget3);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout_2->addWidget(checkBox);


        verticalLayout_7->addLayout(verticalLayout_2);

        checkBox_2 = new QCheckBox(layoutWidget3);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        verticalLayout_7->addWidget(checkBox_2);

        checkBox_3 = new QCheckBox(layoutWidget3);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));

        verticalLayout_7->addWidget(checkBox_3);

        pushButton = new QPushButton(layoutWidget3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"  \n"
"background-color: rgb(191, 63, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(110,42,0,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        verticalLayout_7->addWidget(pushButton);


        retranslateUi(treewindow);

        tabWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(treewindow);
    } // setupUi

    void retranslateUi(QDialog *treewindow)
    {
        treewindow->setWindowTitle(QApplication::translate("treewindow", "Dialog", nullptr));
        label_2->setText(QApplication::translate("treewindow", "                            \320\262\320\270\320\264\321\213 \321\201\320\277\320\276\321\200\321\202\320\260:", nullptr));
        label_3->setText(QApplication::translate("treewindow", "             \321\201\321\202\320\260\320\264\320\270\320\276\320\275\321\213:", nullptr));
        pushButton_2->setText(QApplication::translate("treewindow", "show", nullptr));
        label_4->setText(QApplication::translate("treewindow", "  \320\266\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260 \320\264\320\260\320\275\320\275\321\213\321\205:", nullptr));
        pushButton_4->setText(QApplication::translate("treewindow", "show", nullptr));
        textBrowser->setHtml(QApplication::translate("treewindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("treewindow", "swimming", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("treewindow", "football", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("treewindow", "run", nullptr));
        textBrowser_4->setHtml(QApplication::translate("treewindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("treewindow", "basketball", nullptr));
        pushButton_5->setText(QApplication::translate("treewindow", "show", nullptr));
        label->setText(QApplication::translate("treewindow", "                                                      search", nullptr));
        checkBox->setText(QApplication::translate("treewindow", "stadion", nullptr));
        checkBox_2->setText(QApplication::translate("treewindow", "sportsmen", nullptr));
        checkBox_3->setText(QApplication::translate("treewindow", "jurnal", nullptr));
        pushButton->setText(QApplication::translate("treewindow", "searh", nullptr));
    } // retranslateUi

};

namespace Ui {
    class treewindow: public Ui_treewindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TREEWINDOW_H

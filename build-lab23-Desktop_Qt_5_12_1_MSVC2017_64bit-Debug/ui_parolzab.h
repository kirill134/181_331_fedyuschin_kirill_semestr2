/********************************************************************************
** Form generated from reading UI file 'parolzab.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PAROLZAB_H
#define UI_PAROLZAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_parolzab
{
public:
    QPushButton *pushButton;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEdit;

    void setupUi(QDialog *parolzab)
    {
        if (parolzab->objectName().isEmpty())
            parolzab->setObjectName(QString::fromUtf8("parolzab"));
        parolzab->resize(323, 121);
        pushButton = new QPushButton(parolzab);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(120, 80, 75, 23));
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton->setStyleSheet(QString::fromUtf8("color: white;\n"
"background-color: green;\n"
"border-radius: 5px;\n"
"height: 20px;"));
        widget = new QWidget(parolzab);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(40, 30, 221, 31));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("font: 87 8pt \"Arial Black\";"));

        horizontalLayout->addWidget(label);

        lineEdit = new QLineEdit(widget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setStyleSheet(QString::fromUtf8("border-radius: 5px;"));

        horizontalLayout->addWidget(lineEdit);


        retranslateUi(parolzab);

        QMetaObject::connectSlotsByName(parolzab);
    } // setupUi

    void retranslateUi(QDialog *parolzab)
    {
        parolzab->setWindowTitle(QApplication::translate("parolzab", "Dialog", nullptr));
        pushButton->setText(QApplication::translate("parolzab", "\320\264\320\260\320\273\320\265\320\265", nullptr));
        label->setText(QApplication::translate("parolzab", "\320\272\320\276\320\264\320\276\320\262\320\276\320\265 \321\201\320\273\320\276\320\262\320\276", nullptr));
    } // retranslateUi

};

namespace Ui {
    class parolzab: public Ui_parolzab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PAROLZAB_H

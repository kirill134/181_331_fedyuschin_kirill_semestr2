/********************************************************************************
** Form generated from reading UI file 'secondwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SECONDWINDOW_H
#define UI_SECONDWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_secondwindow
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QTextEdit *textEdit;
    QCheckBox *checkBox;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_3;
    QTextEdit *textEdit_3;
    QPushButton *pushButton_3;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_4;
    QTextEdit *textEdit_4;
    QPushButton *pushButton_4;
    QPushButton *pushButton_2;

    void setupUi(QDialog *secondwindow)
    {
        if (secondwindow->objectName().isEmpty())
            secondwindow->setObjectName(QString::fromUtf8("secondwindow"));
        secondwindow->resize(582, 320);
        secondwindow->setStyleSheet(QString::fromUtf8("background-color: rgb(215, 217, 255);"));
        layoutWidget = new QWidget(secondwindow);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(50, 40, 135, 222));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("font: 75 9pt \"Source Code Pro\";"));
        label_2->setTextFormat(Qt::AutoText);

        verticalLayout->addWidget(label_2);

        textEdit = new QTextEdit(layoutWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout->addWidget(textEdit);

        checkBox = new QCheckBox(layoutWidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout->addWidget(checkBox);

        checkBox_3 = new QCheckBox(layoutWidget);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));

        verticalLayout->addWidget(checkBox_3);

        checkBox_4 = new QCheckBox(layoutWidget);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));

        verticalLayout->addWidget(checkBox_4);

        checkBox_2 = new QCheckBox(layoutWidget);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        verticalLayout->addWidget(checkBox_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));

        verticalLayout->addLayout(horizontalLayout_3);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"background-color: rgb(0, 85, 0);\n"
" border-bottom: 4px solid rgba(1,40,1,1);\n"
" color: #fff;\n"
"height: 20px;"));

        verticalLayout->addWidget(pushButton);

        layoutWidget1 = new QWidget(secondwindow);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(210, 40, 144, 221));
        verticalLayout_6 = new QVBoxLayout(layoutWidget1);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("font: 75 9pt \"Source Code Pro\";"));

        verticalLayout_3->addWidget(label_3);

        textEdit_3 = new QTextEdit(layoutWidget1);
        textEdit_3->setObjectName(QString::fromUtf8("textEdit_3"));
        textEdit_3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout_3->addWidget(textEdit_3);

        pushButton_3 = new QPushButton(layoutWidget1);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"   \n"
"  \n"
"background-color: rgb(0, 85, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(1,40,1,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        verticalLayout_3->addWidget(pushButton_3);


        verticalLayout_6->addLayout(verticalLayout_3);

        layoutWidget2 = new QWidget(secondwindow);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(380, 40, 151, 221));
        verticalLayout_5 = new QVBoxLayout(layoutWidget2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_4 = new QLabel(layoutWidget2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setStyleSheet(QString::fromUtf8("font: 75 9pt \"Source Code Pro\";"));

        verticalLayout_4->addWidget(label_4);

        textEdit_4 = new QTextEdit(layoutWidget2);
        textEdit_4->setObjectName(QString::fromUtf8("textEdit_4"));
        textEdit_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
" border-radius: 5px;"));

        verticalLayout_4->addWidget(textEdit_4);

        pushButton_4 = new QPushButton(layoutWidget2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"   \n"
"  \n"
"background-color: rgb(0, 85, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(1,40,1,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        verticalLayout_4->addWidget(pushButton_4);


        verticalLayout_5->addLayout(verticalLayout_4);

        pushButton_2 = new QPushButton(secondwindow);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(10, 290, 75, 23));
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));
        pushButton_2->setStyleSheet(QString::fromUtf8(" border-radius: 5px;\n"
"\n"
"   \n"
"   \n"
"  \n"
"background-color: rgb(144, 0, 0);\n"
"\n"
"    border-bottom: 4px solid rgba(117,8,1,1);\n"
"    color: #fff;\n"
"height: 20px;"));

        retranslateUi(secondwindow);

        QMetaObject::connectSlotsByName(secondwindow);
    } // setupUi

    void retranslateUi(QDialog *secondwindow)
    {
        secondwindow->setWindowTitle(QApplication::translate("secondwindow", "Dialog", nullptr));
        label_2->setText(QApplication::translate("secondwindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\320\277\320\276\321\200\321\202\321\201\320\274\320\265\320\275\320\260:", nullptr));
        checkBox->setText(QApplication::translate("secondwindow", "swim", nullptr));
        checkBox_3->setText(QApplication::translate("secondwindow", "football", nullptr));
        checkBox_4->setText(QApplication::translate("secondwindow", "run", nullptr));
        checkBox_2->setText(QApplication::translate("secondwindow", "basketball", nullptr));
        pushButton->setText(QApplication::translate("secondwindow", "send", nullptr));
        label_3->setText(QApplication::translate("secondwindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\321\202\320\260\320\264\320\270\320\276\320\275:", nullptr));
        pushButton_3->setText(QApplication::translate("secondwindow", "send", nullptr));
        label_4->setText(QApplication::translate("secondwindow", "\320\226\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260 \320\264\320\260\320\275\320\275\321\213\321\205:", nullptr));
        pushButton_4->setText(QApplication::translate("secondwindow", "send", nullptr));
        pushButton_2->setText(QApplication::translate("secondwindow", "user account", nullptr));
    } // retranslateUi

};

namespace Ui {
    class secondwindow: public Ui_secondwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SECONDWINDOW_H

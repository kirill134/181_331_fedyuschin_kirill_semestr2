#ifndef ADMINQ_H
#define ADMINQ_H

#include <QDialog>

namespace Ui {
class adminq;
}

class adminq : public QDialog
{
    Q_OBJECT

public:
    explicit adminq(QWidget *parent = nullptr);
    ~adminq();

private:
    Ui::adminq *ui;
};

#endif // ADMINQ_H

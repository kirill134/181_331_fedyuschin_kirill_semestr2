#ifndef PAROLZAB_H
#define PAROLZAB_H

#include <QDialog>

namespace Ui {
class parolzab;
}

class parolzab : public QDialog
{
    Q_OBJECT

public:
    explicit parolzab(QWidget *parent = nullptr);
    ~parolzab();

private slots:
    void on_pushButton_clicked();

private:
    Ui::parolzab *ui;
};

#endif // PAROLZAB_H

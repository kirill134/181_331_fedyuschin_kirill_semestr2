#ifndef TREEWINDOW_H
#define TREEWINDOW_H

#include <QDialog>

namespace Ui {
class treewindow;
}

class treewindow : public QDialog
{
    Q_OBJECT

public:
    explicit treewindow(QWidget *parent = nullptr);
    ~treewindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    //void on_pushButton_4_clicked(char newParameter = ';');

    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::treewindow *ui;
};

#endif // TREEWINDOW_H

#ifndef AUTIFI_H
#define AUTIFI_H

#include <QDialog>

namespace Ui {
class autifi;
}

class autifi : public QDialog
{
    Q_OBJECT

public:
    explicit autifi(QWidget *parent = nullptr);
    ~autifi();

private slots:
    void on_pushButton_clicked();

private:
    Ui::autifi *ui;
};

#endif // AUTIFI_H

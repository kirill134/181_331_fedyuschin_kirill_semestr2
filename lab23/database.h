#ifndef DATABASE_H
#define DATABASE_H

#include <QString>
#include <iostream>
#include <vector>

struct sportbasc
{
    QString num;
    QString name;
    QString familye;
};
struct sportfoot
{
    QString num;
    QString name;
    QString familye;
};
struct sportrun
{
    QString num;
    QString name;
    QString familye;
};
struct sportswim
{
    QString num;
    QString name;
    QString familye;
};

class Data
{
public:

    std::vector<sportbasc> bascet;
    std::vector<sportrun> run;
    std::vector<sportswim> swim;
    std::vector<sportfoot> foot;


    Data();
    ~Data();

    void download_all();
    void upload_all();


   std::vector<sportbasc> download_sportbasc();
   //void upload_sportbasc();

  std::vector<sportrun> download_sportrun();
  //  void upload_sportrun();

   std::vector<sportswim> download_sportswim();
   // void upload_sportswim();

  std::vector<sportfoot> download_sportfoot();
  //  void upload_sportfoot();


    //template <class T>
   // void reg(T user, QString lvl_of_access);

   // void login(People auth, QString type);
};

#endif // DATABASE_H

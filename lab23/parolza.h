#ifndef PAROLZA_H
#define PAROLZA_H

#include <QDialog>

namespace Ui {
class parolza;
}

class parolza : public QDialog
{
    Q_OBJECT

public:
    explicit parolza(QWidget *parent = nullptr);
    ~parolza();

private:
    Ui::parolza *ui;
};

#endif // PAROLZA_H

//https://www.openssl.org/docs/man1.1.0/man3/EVP_EncryptInit.html - чистый СИ
//https://wiki.openssl.org/index.php/Libcrypto_API
//https://www.cryptocom.ru/docs/CryptoPack_2.1/libcrypto.pdf
//http://www3.pointsecure.com/doc/hp_ssl_for_vms.pdf

#include <QCoreApplication>

#include <string.h>
#include <stdio.h> // Компилятор MinGW, не MSVC. Первый роднее Qt, второй - Visual Studio.
#include <unistd.h>
#include <openssl/evp.h>

//#define BUFSIZE 1024 // Почти произвольная "порция" данных для шифрования (это не элементарный блок для шифрования), не может превышать EVP_MAX_BLOCK_LENGTH.

/* General encryption and decryption function example using FILE I/O and AES128 with a 128-bit key. */

//Для AES с 256-битный ключом нужно сделать изменения в строках 25, 32, 33.

int do_crypt(FILE *in, FILE *out, int do_encrypt){
    /* Allow enough space in output buffer for additional block */
    unsigned char inbuf[1024], outbuf[1024 + EVP_MAX_BLOCK_LENGTH];
    int inlen, outlen;

    /* Bogus key and IV: we'd normally set these from another source. */
    unsigned char key[] = "0123456789abcdeF"; //16x8 = 128 (бит)
    unsigned char iv[] = "1234567887654321"; //случайное число, всегда 16 байт (по длине элементарного блока)

    EVP_CIPHER_CTX *ctx; // структура для указания алгоритма шифрования и его параметров
    ctx = EVP_CIPHER_CTX_new();

    /* Don't set key or IV right away; we want to check lengths */
    EVP_CipherInit_ex(ctx, EVP_aes_128_cbc(), NULL, NULL, NULL, do_encrypt);
    OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == 16); // проверка размера key на соответствие
    OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16); // проверка размера iv на соответствие
    /* Now we can set key and IV */
    EVP_CipherInit_ex(ctx, NULL, NULL, key, iv, do_encrypt);

    for(;;){
        inlen = fread(inbuf, 1, 1024, in); // 1024 раза по байту из in в inbuf
        if (inlen <= 0) break;
        if(!EVP_CipherUpdate(ctx, outbuf, &outlen, inbuf, inlen)){ // шифрование или дешифрование (зависит от do_encrypt)
            // Error
            EVP_CIPHER_CTX_free(ctx);
            return 0;
        }
        fwrite(outbuf, 1, outlen, out);
    }

    /*if(!EVP_CipherFinal_ex(ctx, outbuf, &outlen)){ // Возвращает результат шифрования/дешифрования последнего неполного блока, если принудительно оключен режим финализации
        // Error
        EVP_CIPHER_CTX_free(ctx);
        return 0;
    }
    fwrite(outbuf, 1, outlen, out);*/

    EVP_CIPHER_CTX_free(ctx); // удаление ctx
    return 1;
}

int main(int argc, char *argv[]){
    QCoreApplication a(argc, argv);

    //############

    //setlocale(LC_ALL, "Russian");

    //Пути по-своему пишите!


    FILE *encode_file = fopen("C:/Users/ELENA/Desktop/t", "rb");
    FILE *decode_file = fopen("C:/Users/ELENA/Desktop/t_enc", "wb");
    do_crypt(encode_file, decode_file, 1); // 0 - decrypt, 1 - encrypt
    fclose(encode_file);
    fclose(decode_file);

    encode_file = fopen("C:/Users/ELENA/Desktop/t_enc", "rb");
    decode_file = fopen("C:/Users/ELENA/Desktop/t_dec", "wb");
    do_crypt(encode_file, decode_file, 0); // 0 - decrypt, 1 - encrypt
    fclose(encode_file);
    fclose(decode_file);

    //############

    return a.exec();
}


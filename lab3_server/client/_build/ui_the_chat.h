/********************************************************************************
** Form generated from reading UI file 'the_chat.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_THE_CHAT_H
#define UI_THE_CHAT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_the_chat
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QLineEdit *text;
    QVBoxLayout *verticalLayout_4;
    QPushButton *exit;
    QPushButton *send;
    QComboBox *comboBox;
    QTableWidget *messages;

    void setupUi(QDialog *the_chat)
    {
        if (the_chat->objectName().isEmpty())
            the_chat->setObjectName(QString::fromUtf8("the_chat"));
        the_chat->resize(586, 396);
        verticalLayoutWidget = new QWidget(the_chat);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(40, 23, 501, 351));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_3->addWidget(label);

        text = new QLineEdit(verticalLayoutWidget);
        text->setObjectName(QString::fromUtf8("text"));

        verticalLayout_3->addWidget(text);


        horizontalLayout_3->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        exit = new QPushButton(verticalLayoutWidget);
        exit->setObjectName(QString::fromUtf8("exit"));

        verticalLayout_4->addWidget(exit);

        send = new QPushButton(verticalLayoutWidget);
        send->setObjectName(QString::fromUtf8("send"));

        verticalLayout_4->addWidget(send);


        horizontalLayout_3->addLayout(verticalLayout_4);


        verticalLayout->addLayout(horizontalLayout_3);

        comboBox = new QComboBox(verticalLayoutWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        verticalLayout->addWidget(comboBox);

        messages = new QTableWidget(verticalLayoutWidget);
        messages->setObjectName(QString::fromUtf8("messages"));

        verticalLayout->addWidget(messages);


        retranslateUi(the_chat);

        QMetaObject::connectSlotsByName(the_chat);
    } // setupUi

    void retranslateUi(QDialog *the_chat)
    {
        the_chat->setWindowTitle(QCoreApplication::translate("the_chat", "\320\247\320\260\321\202", nullptr));
        label->setText(QCoreApplication::translate("the_chat", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\247\320\260\321\202</span></p></body></html>", nullptr));
        text->setPlaceholderText(QCoreApplication::translate("the_chat", "\320\222\320\262\320\265\320\264\320\270 \321\201\320\276\320\276\320\261\321\211\320\265\320\275\320\270\320\265", nullptr));
        exit->setText(QCoreApplication::translate("the_chat", "\320\222\321\213\321\205\320\276\320\264", nullptr));
        send->setText(QCoreApplication::translate("the_chat", "\320\236\321\202\320\277\321\200\320\260\320\262\320\270\321\202\321\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class the_chat: public Ui_the_chat {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_THE_CHAT_H
